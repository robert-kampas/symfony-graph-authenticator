<?php

declare(strict_types=1);

namespace App\Entity;

use SymfonyGraphAuthenticator\GraphAuth\Entity\AbstractUser;

final class User extends AbstractUser
{
}

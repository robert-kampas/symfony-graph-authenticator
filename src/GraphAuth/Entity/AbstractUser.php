<?php

declare(strict_types=1);

namespace SymfonyGraphAuthenticator\GraphAuth\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

abstract class AbstractUser implements UserInterface
{
    /**
     * @ORM\Column(type="string", length=42, unique=true)
     */
    protected $guid;

    /**
     * @ORM\Column(type="string", length=180, nullable=true, unique=true)
     */
    protected $email;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $givenName;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $surname;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $displayName;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $jobTitle;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $officeLocation;

    /**
     * @ORM\Column(type="simple_array", nullable=true)
     */
    protected $roles = [];

    /**
     * @ORM\Column(type="datetime")
     */
    protected $registeredOn;

    /**
     * The last time when user entity was updated with new data from Graph API.
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $updatedOn;

    public function __construct()
    {
        $this->registeredOn = new \DateTime();
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        if ($email === null) {
            $this->email = null;
        } else {
            $this->email = mb_strtolower($email);
        }

        return $this;
    }

    public function getGuid(): string
    {
        return $this->guid;
    }

    public function setGuid(string $guid): self
    {
        $this->guid = $guid;

        return $this;
    }

    public function getGivenName(): ?string
    {
        return $this->givenName;
    }

    public function setGivenName(?string $givenName): self
    {
        $this->givenName = $givenName;

        return $this;
    }

    public function getSurname(): ?string
    {
        return $this->surname;
    }

    public function setSurname(?string $surname): self
    {
        $this->surname = $surname;

        return $this;
    }

    public function getDisplayName(): ?string
    {
        return $this->displayName;
    }

    public function setDisplayName(?string $displayName): self
    {
        $this->displayName = $displayName;

        return $this;
    }

    public function getJobTitle(): ?string
    {
        return $this->jobTitle;
    }

    public function setJobTitle(?string $jobTitle): self
    {
        $this->jobTitle = $jobTitle;

        return $this;
    }

    public function getOfficeLocation(): ?string
    {
        return $this->officeLocation;
    }

    public function setOfficeLocation(?string $officeLocation): self
    {
        $this->officeLocation = $officeLocation;

        return $this;
    }

    public function getRegisteredOn(): \DateTimeInterface
    {
        return $this->registeredOn;
    }

    public function setRegisteredOn(\DateTimeInterface $registeredOn): self
    {
        $this->registeredOn = $registeredOn;

        return $this;
    }

    public function getUpdatedOn(): ?\DateTimeInterface
    {
        return $this->updatedOn;
    }

    public function setUpdatedOn(?\DateTimeInterface $updatedOn): self
    {
        $this->updatedOn = $updatedOn;

        return $this;
    }

    public function getUsername(): string
    {
        return $this->guid;
    }

    public function getUserIdentifier(): string
    {
        return $this->guid;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): ?array
    {
        return $this->roles;
    }

    public function setRoles(?array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * Passwords are not stored in application database.
     *
     * @see UserInterface
     */
    public function getPassword(): ?string
    {
        return null;
    }

    /**
     * Passwords are not checked by application.
     *
     * @see UserInterface
     */
    public function getSalt(): ?string
    {
        return null;
    }

    /**
     * No sensitive data is stored.
     *
     * @see UserInterface
     */
    public function eraseCredentials(): void
    {
        return;
    }
}

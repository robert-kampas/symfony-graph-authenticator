<?php

declare(strict_types=1);

namespace SymfonyGraphAuthenticator\GraphAuth\EventListener;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Security\Http\Event\LogoutEvent;

/**
 * When you want to sign out the user from your app, it isn't sufficient to clear your app's cookies or otherwise end
 * the user's session. You must also redirect the user to the Microsoft identity platform to sign out. If you don't do
 * this, the user reauthenticates to your app without entering their credentials again, because they will have a valid
 * single sign-in session with the Microsoft identity platform.
 *
 * @see https://docs.microsoft.com/en-gb/azure/active-directory/develop/v2-protocols-oidc#send-a-sign-out-request
 */
class LogoutListener
{
    protected $postLogoutRedirectUri;

    public function __construct(?string $postLogoutRedirectUri)
    {
        $this->postLogoutRedirectUri = $postLogoutRedirectUri;
    }

    public function __invoke(LogoutEvent $event): void
    {
        $redirectUrlQuery = $this->postLogoutRedirectUri !== null ? "?post_logout_redirect_uri={$this->postLogoutRedirectUri}" : '';

        $event->setResponse(new RedirectResponse("https://login.microsoftonline.com/common/oauth2/v2.0/logout{$redirectUrlQuery}"));
    }
}

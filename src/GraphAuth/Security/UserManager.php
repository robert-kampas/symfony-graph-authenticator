<?php

declare(strict_types=1);

namespace SymfonyGraphAuthenticator\GraphAuth\Security;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Microsoft\Graph\Model\User as GraphUser;

class UserManager
{
    protected $entityManager;
    protected $firstUserRole;
    protected $defaultUserRole;

    public function __construct(EntityManagerInterface $entityManager, string $firstUserRole, string $defaultUserRole)
    {
        $this->entityManager = $entityManager;
        $this->firstUserRole = $firstUserRole;
        $this->defaultUserRole = $defaultUserRole;
    }

    /**
     * Creates new user from Microsoft Graph user object.
     *
     * @param GraphUser $graphUser
     *
     * @return User new user
     *
     * @throws \Exception
     */
    public function create(GraphUser $graphUser): User
    {
        $role = $this->defaultUserRole;
        $usersExist = $this->entityManager->getConnection()->query('SELECT EXISTS (SELECT 1 FROM user)');

        if (!$usersExist->fetchOne()) {
            $role = $this->firstUserRole;
        }

        $user = new User();
        $user->setGuid($graphUser->getId());
        $user->setRoles([$role]);

        return $this->update($graphUser, $user);
    }

    /**
     * Update user's details.
     *
     * @param GraphUser $graphUser latest Microsoft Graph user object
     * @param User      $user      current application user entity
     *
     * @return User updated user
     *
     * @throws \Exception
     */
    public function update(GraphUser $graphUser, User $user): User
    {
        $givenName = !empty($graphUser->getGivenName()) ? $graphUser->getGivenName() : null;
        $surname = !empty($graphUser->getSurname()) ? $graphUser->getSurname() : null;
        $displayName = !empty($graphUser->getDisplayName()) ? $graphUser->getDisplayName() : null;
        $jobTitle = !empty($graphUser->getJobTitle()) ? $graphUser->getJobTitle() : null;
        $officeLocation = !empty($graphUser->getOfficeLocation()) ? $graphUser->getOfficeLocation() : null;

        $user->setEmail($graphUser->getMail());
        $user->setGivenName($givenName);
        $user->setSurname($surname);
        $user->setDisplayName($displayName);
        $user->setJobTitle($jobTitle);
        $user->setOfficeLocation($officeLocation);
        $user->setUpdatedOn(new \DateTime());

        $this->entityManager->persist($user);
        $this->entityManager->flush();

        return $user;
    }
}

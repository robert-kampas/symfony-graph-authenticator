<?php

declare(strict_types=1);

namespace SymfonyGraphAuthenticator\GraphAuth\Security;

use App\Entity\User;
use League\OAuth2\Client\Provider\GenericProvider;
use Microsoft\Graph\Graph;
use Microsoft\Graph\Model\User as GraphUser;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Guard\AbstractGuardAuthenticator;
use SymfonyGraphAuthenticator\GraphAuth\Repository\UserRepository;

class GraphAuthenticator extends AbstractGuardAuthenticator
{
    protected $session;
    protected $userRepository;
    /** @var UserManager used to create and update user entity */
    protected $userManager;
    protected $applicationId;
    protected $applicationSecret;
    protected $directoryId;
    protected $callbackUri;
    protected $scopes;

    public function __construct(SessionInterface $session, UserRepository $userRepository, UserManager $userManager, string $applicationId, string $applicationSecret, string $directoryId, string $callbackUri, array $scopes)
    {
        $this->session = $session;
        $this->userRepository = $userRepository;
        $this->userManager = $userManager;
        $this->applicationId = $applicationId;
        $this->applicationSecret = $applicationSecret;
        $this->directoryId = $directoryId;
        $this->callbackUri = $callbackUri;
        $this->scopes = $scopes;
    }

    /**
     * Authenticator is supported only if all request query parameters and session value is set.
     *
     * {@inheritdoc}
     */
    public function supports(Request $request): bool
    {
        return $request->query->has('code') &&
            $request->query->has('state') &&
            $this->session->get('oauth_state', false);
    }

    /**
     * Get code used in callback to get access token.
     *
     * {@inheritdoc}
     */
    public function getCredentials(Request $request): string
    {
        return $request->query->get('code');
    }

    /**
     * Returns true if user access token is present.
     *
     * {@inheritdoc}
     */
    public function checkCredentials($credentials, UserInterface $user): bool
    {
        return $this->session->has('access_token');
    }

    /**
     * Get user's details from Microsoft Graph and find user in application database. When user is not found in
     * the database then new user is created.
     *
     * {@inheritdoc}
     */
    public function getUser($credentials, UserProviderInterface $userProvider): ?User
    {
        $oauthClient = new GenericProvider([
            'clientId' => $this->applicationId,
            'clientSecret' => $this->applicationSecret,
            'redirectUri' => $this->callbackUri,
            'urlAuthorize' => "https://login.microsoftonline.com/{$this->directoryId}/oauth2/v2.0/authorize",
            'urlAccessToken' => "https://login.microsoftonline.com/{$this->directoryId}/oauth2/v2.0/token",
            'urlResourceOwnerDetails' => null,
            'scopes' => $this->scopesParser($this->scopes),
        ]);

        try {
            /** @var string $accessToken */
            $accessToken = $oauthClient->getAccessToken('authorization_code', [
                'code' => $credentials,
            ]);

            $graph = new Graph();
            $graph->setAccessToken($accessToken);
            /** @var GraphUser $graphUser */
            $graphUser = $graph->createRequest('GET', '/me')
                ->setReturnType(GraphUser::class)
                ->execute();
            $this->session->set('access_token', $accessToken);

            // Find user by Graph id
            $user = $this->userRepository->findOneBy(['guid' => $graphUser->getId()]);

            if (null === $user) {
                // If user was not found then create new user
                $user = $this->userManager->create($graphUser);
            } else {
                // If user was found then update user's details because we already have latest user data from graph
                $user = $this->userManager->update($graphUser, $user);
            }
        } catch (\Exception $e) {
            throw new AuthenticationException($e->getMessage());
        }

        return $user;
    }

    /**
     * Called when authentication executed, but failed (e.g. wrong username password).
     *
     * {@inheritdoc}
     */
    public function onAuthenticationFailure(Request $request, AuthenticationException $exception): Response
    {
        $this->session->remove('oauth_state');

        return new Response($exception->getMessageKey(), Response::HTTP_FORBIDDEN);
    }

    /**
     * Redirect user back to original page.
     *
     * {@inheritdoc}
     */
    public function onAuthenticationSuccess(Request $request, TokenInterface $token, string $providerKey): RedirectResponse
    {
        $return_uri = $this->session->get('return_uri', '/');
        $this->session->remove('return_uri');
        $this->session->remove('oauth_state');

        return new RedirectResponse($return_uri, Response::HTTP_FOUND);
    }

    /**
     * Returns a response that directs the user to authenticate. This is called when an anonymous request accesses a
     * resource that requires authentication.
     *
     * {@inheritdoc}
     */
    public function start(Request $request, ?AuthenticationException $authException = null): RedirectResponse
    {
        // After successful authentication redirect user back to the same page where they were.
        $this->session->set('return_uri', $request->getRequestUri());

        $oauthClient = new GenericProvider([
            'clientId' => $this->applicationId,
            'clientSecret' => $this->applicationSecret,
            'redirectUri' => $this->callbackUri,
            'urlAuthorize' => "https://login.microsoftonline.com/{$this->directoryId}/oauth2/v2.0/authorize",
            'urlAccessToken' => "https://login.microsoftonline.com/{$this->directoryId}/oauth2/v2.0/token",
            'urlResourceOwnerDetails' => null,
            'scopes' => $this->scopesParser($this->scopes),
        ]);
        $authUrl = $oauthClient->getAuthorizationUrl();
        // Save client state so we can validate in callback
        $this->session->set('oauth_state', $oauthClient->getState());

        return new RedirectResponse($authUrl, Response::HTTP_FOUND);
    }

    /**
     * Feature not used.
     *
     * {@inheritdoc}
     */
    public function supportsRememberMe(): bool
    {
        return false;
    }

    /**
     * Formats provided and adds missing mandatory scopes.
     *
     * @return string containing Graph application scopes
     */
    protected function scopesParser(array $scopes): string
    {
        $scopes = array_map(
            function ($scope) {
                return mb_strtolower($scope);
            },
            $scopes
        );

        if (!in_array('openid', $scopes)) {
            $scopes[] = 'openid';
        }

        if (!in_array('offline_access', $scopes)) {
            $scopes[] = 'offline_access';
        }

        if (!in_array('profile', $scopes)) {
            $scopes[] = 'profile';
        }

        if (!in_array('user.read', $scopes)) {
            $scopes[] = 'user.read';
        }

        return implode(' ', $scopes);
    }
}

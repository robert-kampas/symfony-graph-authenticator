<?php

namespace SymfonyGraphAuthenticator\GraphAuth\Security;

use App\Entity\User;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use SymfonyGraphAuthenticator\GraphAuth\Repository\UserRepository;

class UserProvider implements UserProviderInterface
{
    protected $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * Symfony calls this method if you use features like switch_user or remember_me.
     *
     * @param string $username user's Graph id
     *
     * @return User
     */
    public function loadUserByUserIdentifier($username): User
    {
        $user = $this->userRepository->findOneBy(['guid' => $username]);

        if ($user === null) {
            throw new UsernameNotFoundException();
        }

        return $user;
    }

    /**
     * {@inheritDoc}
     */
    public function loadUserByUsername($username): User
    {
        return $this->loadUserByUserIdentifier($username);
    }

    /**
     * Refreshes the user after being reloaded from the session.
     *
     * @param UserInterface $user
     *
     * @return User fresh user object
     */
    public function refreshUser(UserInterface $user): User
    {
        if (!$user instanceof User) {
            throw new UnsupportedUserException(sprintf('Invalid user class "%s".', get_class($user)));
        }

        $freshUser = $this->userRepository->findOneBy(['guid' => $user->getGuid()]);

        if ($freshUser === null) {
            throw new UsernameNotFoundException();
        }

        return $freshUser;
    }

    public function supportsClass(string $class): bool
    {
        return User::class === $class;
    }
}

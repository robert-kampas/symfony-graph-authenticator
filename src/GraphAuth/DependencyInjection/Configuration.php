<?php

declare(strict_types=1);

namespace SymfonyGraphAuthenticator\GraphAuth\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    /**
     * @psalm-suppress PossiblyUndefinedMethod
     * @psalm-suppress MissingClosureParamType
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder('graph_auth', 'array');
        $treeBuilder->getRootNode()
            ->children()
                ->variableNode('application_id')
                    ->isRequired()
                    ->validate()
                        ->ifTrue(function ($value) {
                            return !is_string($value);
                        })
                        ->thenInvalid('application_id must be string.')
                    ->end()
                ->end()
                ->variableNode('application_secret')
                    ->isRequired()
                    ->validate()
                        ->ifTrue(function ($value) {
                            return !is_string($value);
                        })
                        ->thenInvalid('application_secret must be string.')
                    ->end()
                ->end()
                ->variableNode('directory_id')
                    ->isRequired()
                    ->validate()
                        ->ifTrue(function ($value) {
                            return !is_string($value);
                        })
                        ->thenInvalid('directory_id must be string.')
                    ->end()
                ->end()
                ->variableNode('callback_uri')
                    ->isRequired()
                    ->validate()
                        ->ifTrue(function ($value) {
                            return !is_string($value);
                        })
                        ->thenInvalid('callback_uri must be string.')
                    ->end()
                ->end()
                ->variableNode('post_logout_redirect_uri')
                    ->defaultNull()
                    ->validate()
                        ->ifTrue(function ($value) {
                            return !is_string($value) && $value !== null;
                        })
                        ->thenInvalid('post_logout_redirect_uri must be string.')
                    ->end()
                ->end()
                ->arrayNode('scopes')
                    ->defaultValue([])
                    ->cannotBeEmpty()
                    ->variablePrototype()
                        ->cannotBeEmpty()
                    ->end()
                ->end()
                ->variableNode('first_user_role')
                    ->defaultValue('ROLE_SUPER_ADMIN')
                    ->cannotBeEmpty()
                    ->validate()
                        ->ifTrue(function ($value) {
                            return !is_string($value);
                        })
                        ->thenInvalid('first_user_role must be string.')
                    ->end()
                ->end()
                ->variableNode('default_user_role')
                    ->defaultValue('ROLE_USER')
                    ->cannotBeEmpty()
                    ->validate()
                        ->ifTrue(function ($value) {
                            return !is_string($value);
                        })
                        ->thenInvalid('default_user_role must be string.')
                    ->end()
                ->end()
            ->end();

        return $treeBuilder;
    }
}
